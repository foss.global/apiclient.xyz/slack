"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackAccount = void 0;
const plugins = __importStar(require("./slack.plugins"));
class SlackAccount {
    constructor(slackTokenArg) {
        this.postUrl = 'https://slack.com/api/chat.postMessage';
        this.updateUrl = 'https://slack.com/api/chat.update';
        this.slackToken = slackTokenArg;
    }
    async sendMessage(optionsArg) {
        let requestBody = {
            channel: optionsArg.channelArg,
            text: optionsArg.messageOptions.text
        };
        if (optionsArg.messageOptions.fields) {
            requestBody = Object.assign(Object.assign({}, requestBody), { attachments: [
                    {
                        pretext: optionsArg.messageOptions.pretext,
                        fields: optionsArg.messageOptions.fields,
                        ts: optionsArg.messageOptions.ts,
                        color: optionsArg.messageOptions.color
                    }
                ] });
        }
        let postUrl = this.postUrl;
        switch (true) {
            case optionsArg.ts && optionsArg.mode === 'update':
                requestBody = Object.assign(Object.assign({}, requestBody), { ts: optionsArg.ts });
                postUrl = this.updateUrl;
                break;
            case optionsArg.ts && optionsArg.mode === 'threaded':
                requestBody = Object.assign(Object.assign({}, requestBody), { thread_ts: optionsArg.ts });
                break;
        }
        const response = await plugins.smartrequest.postJson(postUrl, {
            headers: {
                Authorization: `Bearer ${this.slackToken}`
            },
            requestBody
        });
        return response;
    }
}
exports.SlackAccount = SlackAccount;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xhY2suY2xhc3Nlcy5zbGFja2FjY291bnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9zbGFjay5jbGFzc2VzLnNsYWNrYWNjb3VudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseURBQTJDO0FBRzNDLE1BQWEsWUFBWTtJQUl2QixZQUFZLGFBQXFCO1FBSHpCLFlBQU8sR0FBRyx3Q0FBd0MsQ0FBQztRQUNuRCxjQUFTLEdBQUcsbUNBQW1DLENBQUM7UUFHdEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7SUFDbEMsQ0FBQztJQUVELEtBQUssQ0FBQyxXQUFXLENBQUMsVUFLakI7UUFDQyxJQUFJLFdBQVcsR0FBUTtZQUNyQixPQUFPLEVBQUUsVUFBVSxDQUFDLFVBQVU7WUFDOUIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxjQUFjLENBQUMsSUFBSTtTQUNyQyxDQUFDO1FBRUYsSUFBSSxVQUFVLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRTtZQUNwQyxXQUFXLG1DQUNOLFdBQVcsS0FDZCxXQUFXLEVBQUU7b0JBQ1g7d0JBQ0UsT0FBTyxFQUFFLFVBQVUsQ0FBQyxjQUFjLENBQUMsT0FBTzt3QkFDMUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxjQUFjLENBQUMsTUFBTTt3QkFDeEMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxjQUFjLENBQUMsRUFBRTt3QkFDaEMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxjQUFjLENBQUMsS0FBSztxQkFDdkM7aUJBQ0YsR0FDRixDQUFDO1NBQ0g7UUFFRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBRTNCLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxVQUFVLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxJQUFJLEtBQUssUUFBUTtnQkFDaEQsV0FBVyxtQ0FDTixXQUFXLEtBQ2QsRUFBRSxFQUFFLFVBQVUsQ0FBQyxFQUFFLEdBQ2xCLENBQUM7Z0JBQ0YsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ3pCLE1BQU07WUFDUixLQUFLLFVBQVUsQ0FBQyxFQUFFLElBQUksVUFBVSxDQUFDLElBQUksS0FBSyxVQUFVO2dCQUNsRCxXQUFXLG1DQUNOLFdBQVcsS0FDZCxTQUFTLEVBQUUsVUFBVSxDQUFDLEVBQUUsR0FDekIsQ0FBQztnQkFDRixNQUFNO1NBQ1Q7UUFFRCxNQUFNLFFBQVEsR0FBRyxNQUFNLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRTtZQUM1RCxPQUFPLEVBQUU7Z0JBQ1AsYUFBYSxFQUFFLFVBQVUsSUFBSSxDQUFDLFVBQVUsRUFBRTthQUMzQztZQUNELFdBQVc7U0FDWixDQUFDLENBQUM7UUFDSCxPQUFPLFFBQVEsQ0FBQztJQUNsQixDQUFDO0NBQ0Y7QUEzREQsb0NBMkRDIn0=